package com.example.miguelparra.todo_list

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_list.*

import java.util.*

class ActivityList : AppCompatActivity(),
        ListSelectionRecyclerViewAdapter.ListSelectionRecyclerViewClickListener
{
    //7L14qu nuestro activity implemente la interfaz creada

    lateinit var listsRecyclerView: RecyclerView //L

    val database = FirebaseDatabase.getInstance()  // L7 creando una instancia de firebase database
    val ref = database.getReference("todo-list")// L7 referencia a un path llamado to do list



    companion object { //2L14
        val INTENT_LIST_ID = "listID"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        setSupportActionBar(toolbar)

//RECLICLER VIEW L

        listsRecyclerView = findViewById(R.id.list_recycler_view) //L Conectar con la lista
        listsRecyclerView.layoutManager  = LinearLayoutManager(this) //L le configuro con un layout manager de linea, para que se vea horizontal
        listsRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref, this) //L aqui creamos un adapter //8l14


        fab.setOnClickListener { view ->
            showCreateListDialog()
            /*  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                      .setAction("Action", null).show()*/
        }
    }

    private fun showCreateListDialog(){

        val dialogTitle =getString(R.string.name_of_list) // sacando los strings
        val positiveButtonTitle =  getString(R.string.create_list) // .. sacando los strings

        val  builder = AlertDialog.Builder(this) // builder para nuestro alerta
        val listTitleEditText = EditText(this) // creando una vista de tipo edit texto
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT

        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle) {dialog, i ->

            val newList= listTitleEditText.text.toString() //L7
            val newId = UUID.randomUUID().toString()
            //ref.child(newId)
            // ref.setValue(newList) //L7
            ref.child(newId).child("list-name").setValue(newList)//L7 con el .child, estamos creando como una indentacion mas en la base de datos // esto base de datos por archivo
            //siempre que queremos geerear id con el codigo usamos ese UUID que es un algoritmo raro que saca un id que no se va a repetir ,
            //dentro de ese vamos a crear un list-name que es la llave del valor que voy a setear en newList

            dialog.dismiss()
            showListDetail(newId)
        }

        builder.create().show()

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_activity_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun showListDetail(listId:String){ //1L14
        val listDetailIntent = Intent(this, ListDetailActivity::class.java) //crear el intent, estoy en este activity y quiero ir al otro activity

        listDetailIntent.putExtra(INTENT_LIST_ID, listId) //puedo enviar el id al siguiente activity, es decir nos permite enviar informacion a otro actibity // la llamamos cuando toquemos un item de la lista

        startActivity(listDetailIntent) //lanza la siguiente actividad o activity


    }

    //lEGADO: TU MANEJAS EL CLIC YO NO
    override fun listenerItemClicked(todoList: TodoList) { //que queremos hacer cuando le demos clic en cada uno de los items
        showListDetail(todoList.id)
    }

}
