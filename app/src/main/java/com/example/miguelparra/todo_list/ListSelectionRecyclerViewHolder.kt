package com.example.miguelparra.todo_list


import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class ListSelectionRecyclerViewHolder(itemView:View): //mi clase necesita ese atributo
        RecyclerView.ViewHolder(itemView) { //tipo de clase (en este caso de tipo view holder) que queremos crear y pasamos la inicializacion del atributo viewHolder

    val listTitle = itemView.findViewById<TextView>(R.id.itemString) //ligando el string creado en el layout
}