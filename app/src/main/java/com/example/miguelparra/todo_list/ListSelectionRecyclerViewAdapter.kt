package com.example.miguelparra.todo_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import java.util.*


class ListSelectionRecyclerViewAdapter(
        ref:DatabaseReference,
        val clickListener: ListSelectionRecyclerViewClickListener ): //5L14
        RecyclerView.Adapter<ListSelectionRecyclerViewHolder>(){

    //4L14  Patron de disenio (?): De Legado
    interface ListSelectionRecyclerViewClickListener {
        fun listenerItemClicked(todoList: TodoList)
    }


    //val listTitles = arrayOf("Shopping List","Chores","Homework") // L datos quemasdos a mostrar en la tabla
    val todoLists:ArrayList<TodoList> = arrayListOf()

    init {

        ref.addChildEventListener(object : ChildEventListener{ ////addClid tiene algunos eventos que necesitamos implementat

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onChildRemoved(item: DataSnapshot) { //cuando se elimina nos devuelve u daro en est funcion
                val deletedIndex = todoLists.indexOfFirst { it.id == item.key} //estoy encontrando el indice del key que se elimino// est} todoList.remove()b//id como lollamamos en kotlin y key eso largaso
                //indexOfFirs se ejecuta muchas veces, se busca en muchos items esa codicion
                // otra forma de escribir lo anterior es indexOfFirst{ element -> element.id == item.key
                todoLists.removeAt(deletedIndex)
                notifyItemRemoved(deletedIndex)
            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val listTitle = item.child("list-name").value.toString() //tengo la refencia en item a la referencia de firebase
                val listId = item.key.toString()

                todoLists.add(TodoList(listId, listTitle))
                notifyItemInserted(todoLists.size) //para decirle al adapter que su data cambio para que se actualice

            }

        })

    }
    override fun onCreateViewHolder( //retornando un view holder del tipo que queramos
            parent: ViewGroup,
            viewType: Int): ListSelectionRecyclerViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_selection_view_holder, parent, false) //con las 2 lineas, genero una instancia para cada uno de los view holdrs y retornamos el view selection creado, //mcreando la lista con el layout creado

        return ListSelectionRecyclerViewHolder(view)

    }

    override fun getItemCount(): Int {
        return todoLists.count()
    }


    override fun onBindViewHolder( //en este caso se va a repetir tres veces o el numero de veces de acuerdo a los items que voy a presentar
            holder: ListSelectionRecyclerViewHolder,
            position: Int) { //estamos poniendo la data
        holder.listTitle.text = todoLists[position].listName //va a tener la referencia de cada items que voy a presentar

        holder.itemView.setOnClickListener {
            clickListener.listenerItemClicked(todoLists[position]) //6L14
        }
    }
}


/*

package com.example.miguelparra.todo_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import java.text.FieldPosition

class ListSelectionRecyclerViewAdapter:
        RecyclerView.Adapter<ListSelectionRecyclerViewHolder>() {

    val listTitles = arrayOf("Shopping List","Chores","Homework")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListSelectionRecyclerViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_selection_view_holder,parent,false)//genrar una insancia para cada uno de los view holders
        return ListSelectionRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {//devolvemos cuantas filas voya tener...es decir el nuemro de elementos del arrelos
        return listTitles.count()//To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: ListSelectionRecyclerViewHolder, position: Int) {
        holder.listTitle.text= listTitles[position] //se va a repetir el nuermo de veces qde items que tengamos

    }

}


*/
